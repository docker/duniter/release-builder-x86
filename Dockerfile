# Image for Duniter releases on Linux.
#
# Building this image:
#   docker build . -t duniter/release-builder-x86

FROM i686/ubuntu:14.04
MAINTAINER Élois <elois@duniter.org>

ENV DEBIAN_FRONTEND noninteractive

# Install needed tools
RUN apt-get update && \
	apt-get install -y apt-transport-https curl && \
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
	echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
	apt-get update && \
	apt-get install -y build-essential yarn python-minimal zip && \
	apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create compiling user
RUN mkdir /duniter && \
	adduser --system --group --quiet --shell /bin/bash --home /duniter duniter && \
	chown duniter:duniter /duniter
WORKDIR /duniter

# Load NVM
ENV NVM_DIR "/duniter/.nvm"
USER duniter
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash
RUN echo "export NVM_DIR=$NVM_DIR" >/duniter/.profile
USER root

# Entry point
COPY bootstrap.sh /root/bootstrap.sh
RUN chmod u+x /root/bootstrap.sh
ENTRYPOINT ["/root/bootstrap.sh"]
